def change (money):
    coin =[]
    coin.append(money // 10)
    money = money % 10
    coin.append(money // 5)
    money = money % 5
    coin.append(money // 4)
    money = money % 4
    coin.append(money // 2)
    money = money % 2
    coin.append(money // 1)
    money = money % 1

    return coin

if __name__ == "__main__":
    money = int(input("Enter your money:"))
    coins = change (money)
    print("coin 10:", coins[0],"coin")
    print("coin 5:", coins[1],"coin")
    print("coin 4:", coins[2],"coin")
    print("coin 2:", coins[3],"coin")
    print("coin 1:", coins[4],"coin")


