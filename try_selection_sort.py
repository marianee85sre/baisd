def selection_sort(number: list)->list:
    sort_num = number.copy()

    for i in range  (len(number) -1,0,-1 ):
        max_num = 0
        for j in range (1, i + 1):
            if sort_num[j] > sort_num[max_num]:
                max_num = j 

        sort_num[i],sort_num[max_num] = sort_num[max_num],sort_num[i]
    return sort_num
            

if __name__=="__main__":
    number = list(map(int,input("Enter your number: ").split()))
    sort_num = selection_sort(number)
    print(sort_num)